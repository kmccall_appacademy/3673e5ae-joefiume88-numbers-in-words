class Fixnum
  def in_words

    if self < 10
      ones_place(self)
    elsif self < 20
      teens_place(self)
    elsif self % 10 == 0 && self < 100
      tens_place(self)
    elsif self < 100
      hundreds_place(self)
    elsif self < 1000
      hundreds_place(self)
    elsif self < 1_000_000
      thousands_place(self)
    elsif self < 1_000_000_000
      millions_place(self)
    elsif self < 1_000_000_000_000
      billions_place(self)
    else
      trillions_place(self)
    end
  end

  private

  def ones_place(num)
    ones = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
    ones[num]
  end

  def tens_place(num)
    tens = ['ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']
    tens[num / 10 - 1]
  end

  def teens_place(num)
    teens = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']
    teens[num - 10]
  end

  def hundreds_place(num)
    if num < 10
      ones_place(num)
    elsif num < 20
      teens_place(num)
    elsif num < 100 && num % 10 == 0
      tens_place(num)
    elsif num < 100
      tens_place(num) + ' ' + ones_place(num % 10)
    elsif num % 100 == 0
      ones_place(num / 100) + ' hundred'
    elsif num % 100 < 20
      ones_place(num / 100) + ' hundred ' + teens_place(num % 100)
    elsif num.to_s[-2..-1] == '00'
      ones_place(num / 100) + ' hundred'
    elsif num.to_s[-1] == '0'
      ones_place(num / 100) + ' hundred ' + tens_place(num % 100)
    else
      ones_place(num / 100) + ' hundred ' + tens_place(num % 100) + ' ' + ones_place(num % 100 % 10)
    end
  end

  def thousands_place(num)
    if num < 1_000
      hundreds_place(num)
    elsif num % 1000 == 0
      ones_place(num / 1000) + ' thousand'
    else
      hundreds_place(num / 1000) + ' thousand ' + hundreds_place(num % 1000)
    end
  end

  def millions_place(num)
    if num < 1_000_000
      thousands_place(num)
    elsif num % 1_000_000 == 0
      ones_place(num / 1_000_000) + ' million'
    else
      hundreds_place(num / 1_000_000) + ' million ' + thousands_place(num % 1_000_000)
    end
  end

  def billions_place(num)
    if num < 1_000_000_000
      millions_place(num)
    elsif num % 1_000_000_000 == 0
      ones_place(num / 1_000_000_000) + ' billion'
    else
      hundreds_place(num / 1_000_000_000) + ' billion ' + millions_place(num % 1_000_000_000)
    end
  end

  def trillions_place(num)
    if num < 1_000_000_000_000
      billions_place(num)
    elsif num % 1_000_000_000_000 == 0
      ones_place(num / 1_000_000_000_000) + ' trillion'
    else
      hundreds_place(num / 1_000_000_000_000) + ' trillion ' + billions_place(num % 1_000_000_000_000)
    end
  end


end
